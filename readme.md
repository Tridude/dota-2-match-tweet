Dota 2 Match Tweet
==============

This is a sub heading
--------------

A Python program that will post on Twitter about recent matches for a specified public player.
The player's Dota 2 setting/account must be set to public (not Steam profile).

Required modules:
python-twitter https://github.com/bear/python-twitter
ConfigObj http://www.voidspace.org.uk/python/configobj.html
lxml: http://lxml.de/
   Unofficial Windows binaries: http://www.lfd.uci.edu/~gohlke/pythonlibs/#lxml

You will also need to set your steam api key, steam 32bit ID, and twitter api keys in the .cfg file.