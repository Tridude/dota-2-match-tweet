import sys
import twitter
import urllib2
import gzip
import datetime
from configobj import ConfigObj
from time import sleep
from StringIO import StringIO
from lxml import etree as ET

# Check parameter for optional .cfg file name.
# If no parameter, default to d2tconfig.cfg
cfgFile = ''
if len(sys.argv) > 1:
   cfgFile = sys.argv[1]
else:
   cfgFile = 'd2tconfig.cfg'

# Global vars
MAX_URL_TRIES = 3
config = ConfigObj(cfgFile)
lastMatchID = config['lastMatchID']
steamKey = config['steamKey']
#steamID64 = config['steamID64']
steamID32 = config['steamID32']
tConsumerKey = config['tConsumerKey']
tConsumerSecret = config['tConsumerSecret']
tAccessTokenKey = config['tAccessTokenKey']
tAccessTokenSecret = config['tAccessTokenSecret']
lobbyTypes = {
   '-1':'Invalid',
   '0': 'Matchmaking',
   '1': 'Practice',
   '2': 'Tournament',
   '3': 'Tutorial',
   '4': 'Co-op vs AI',
   '5': 'Team match',
   }
gameModes = {
   '1' : 'All Pick',
   '2' : 'Captains Mode',
   '3' : 'Random Draft',
   '4' : 'Single Draft',
   '5' : 'All Random',
   '6' : 'INTRO/DEATH',
   '7' : 'The Diretide',
   '8' : 'Reverse Captains Mode',
   '9' : 'Greeviling',
   '10' : 'Tutorial',
   '11' : 'Mid Only',
   '12' : 'Least Played',
   '13' : 'New Player Pool'
   }
print "Dota2 last: " + lastMatchID

# requestURL ex: 'IDOTA2Match_570/GetMatchHistory/V001/?matches_requested=25'
# returns XML root node
def requestDota(requestURL):
   done = False
   tries = 0
   while not done and tries < MAX_URL_TRIES:
      tries += 1
      # sleep(1) makes sure less than 1 request is sent per 1 second.
      sleep(1)
      try:
         request = urllib2.Request('https://api.steampowered.com/' + requestURL + '&format=XML&key=' + steamKey)
         # Accepts gzip encoded respose
         request.add_header('Accept-encoding', 'gzip')
         response = urllib2.urlopen(request)
         # Handle the request if gzip encoded
         if response.info().get('Content-Encoding') == 'gzip':
            buff = StringIO(response.read())
            xmlStr = gzip.GzipFile(fileobj=buff)
            root = ET.fromstring(xmlStr.read())
         else:
            root = ET.fromstring(response.read())
         response.close()
         done = True
      except urllib2.HTTPError, e:
         print "HTTP Error: " + str(e)
      except urllib2.URLError, e:
         print "URL Error: " + str(e)
   if tries == MAX_URL_TRIES:
      print "Too many URL retries"
      sys.exit(1)
   return root

# heroName
# accepts heroID as string, returns the name of hero associated to the heroID as string
def heroName(heroID):
   #hero = heroRoot.find(".//hero["+heroID+"]").find('localized_name').text # doesn't work as heroID is not necessarily the index in the XML.
   hero = ''
   # Iterates through heroRoot XML. There should be a better way using XML functions.
   for heroEle in heroRoot.iter('hero'):
      if heroEle.find('id').text == heroID:
         hero = heroEle.find('localized_name').text
         break
   return hero

# Parameter: lobbyType as int
#def getLobbyType(lobbyType):
#   return lobbyTypes.get(lobbyType, 'N/A')

#def getGameMode(mode):
#   return gameModes.get(mode, 'N/A')

# Initiate twitter API object
api = twitter.Api(
   consumer_key = tConsumerKey,
   consumer_secret = tConsumerSecret,
   access_token_key = tAccessTokenKey,
   access_token_secret = tAccessTokenSecret
)

# Request recent matches for profile.
root = requestDota('IDOTA2Match_570/GetMatchHistory/V001/?matches_requested=10&account_id='+steamID32)
# Check if there are any matches.
if root.find('num_results').text == '0':
   print "No matches were found\n"
   sys.exit(1)
# Initialize heroRoot XML
heroRoot = requestDota('IEconDOTA2_570/GetHeroes/v0001/?language=en_us')


# Go through matches from oldest to newest.
for match in reversed(root.findall('./matches/match')):
   # Get match ID of current match. If less than last match, skip.
   matchID = match.find('match_id').text
   if int(matchID) <= int(lastMatchID):
      continue
   #matchSeqNum = match.find('match_seq_num').text # Unused
   # Start time in Unix Timestamp. Convert to human readable.
   startTime = match.find('start_time').text
   timeText = datetime.datetime.fromtimestamp(int(startTime)).strftime('%Y/%m/%d %H:%M:%S')
   # Get lobby type in integer, then get the associated word.
   lobbyType = match.find('lobby_type').text
   lobbyTypeName = lobbyTypes.get(lobbyType, 'N/A')

   # Find your player using 32 bit steam ID
   player = match.xpath('./players/player/account_id[text()='+str(steamID32)+']/..')
   # Can't find your player. Should not happen.
   if player == None or len(player) == 0:
      continue
   # Get slot of player and find out side.
   slot = player[0].find('player_slot').text
   if int(slot) < 127:
      side = 'Radiant'
   else:
      side = 'Dire'
   # Get hero ID and convert to hero name.
   heroID = player[0].find('hero_id').text
   hero = heroName(heroID)
   # Make twitter message for match.
   twitterMessage = 'Dota 2 - Match ID ' + matchID + ' ' + lobbyTypeName + ' started ' + timeText + '. Played ' + hero + ' on ' + side + '. #dota2 #dota2api'
   print len(twitterMessage),twitterMessage
   status = api.PostUpdate(twitterMessage[:140])
   #print status.text
   # If lobby is public matchmaking, get details. Untested.
   if lobbyType == '0':
      matchRoot = requestDota('IDOTA2Match_570/GetMatchDetails/V001/?match_id=matchID')
      player = matchRoot.xpath('.//player/account_id[text()='+str(steamID32)+']/..')
      if player == None:
         continue
      player = player[0]
      kills = player.find('kills').text
      deaths = player.find('deaths').text
      assists = player.find('assists').text
      #lastHits = player.find('last_hits').text
      #denies = player.find('denies').text
      gpm = player.find('gold_per_min').text
      xpm = player.find('xp_per_min').text
      #level = player.find('level').text
      #item0 = player.find('item_0').text # No current API to identify items. Can be identified with local file.
      #item1 = player.find('item_1').text
      #item2 = player.find('item_2').text
      #item3 = player.find('item_3').text
      #item4 = player.find('item_4').text
      #item5 = player.find('item_5').text
      mode = matchRoot.find('game_mode').text
      modeName = gameModes.get(mode, 'N/A')
      radiantWin = matchRoot.find('radiant_win').text
      #duration = matchRoot.find('duration').text
      #towerRad = matchRoot.find('tower_status_radiant').text
      #towerDir = matchRoot.find('tower_status_dire').text
      #barrackRad = matchRoot.find('barracks_status_radiant').text
      #barrackDir = matchRoot.find('barracks_status_dire').text
      #firstBlood = matchRoot.find('first_blood_time').text
      twitterMessage = 'Cont. Match ID ' + matchID + ' ' + modeName + ' ' + kills + ' kills ' + deaths + ' deaths ' + assists + ' assists ' + gpm + ' gpm ' + xpm + ' xpm. #dota2 #dota2api'
      print len(twitterMessage), twitterMessage
      status = api.PostUpdate(twitterMessage[:140])
      #print status.text

if matchID == config['lastMatchID']:
   print "No new matches found"
else:
   config['lastMatchID'] = matchID
   config.write()